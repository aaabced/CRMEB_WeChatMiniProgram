//如果独立站点部署请配置新的URL
//整站接口请求地址
let API_URL = ''
// 管理后台 socket 通信地址
let WS_ADMIN_URL = ''
// 客服 socket 通信地址
let WS_KEFU_URL = ''

if (process.env.NODE_ENV === 'development') {
    //测试环境默认请求演示站数据
    API_URL = `http://demomin.crmeb.net/adminapi`
    WS_ADMIN_URL = `ws:demomin.crmeb.net:20082`
    WS_KEFU_URL = `ws:demomin.crmeb.net:20083`

}else{
    // 生产环境请求接口地址 如果没有配置自动获取当前网址路径，如果独立部署请自行配置请求URL
    API_URL = API_URL || `${location.origin}/adminapi`
    WS_ADMIN_URL = WS_ADMIN_URL || `ws:${location.hostname}:20002`
    WS_KEFU_URL = WS_KEFU_URL || `ws:${location.hostname}:20003`
}

const VUE_APP_API_URL = API_URL
const VUE_APP_WS_ADMIN_URL = WS_ADMIN_URL
const VUE_APP_WS_KEFU_URL = WS_KEFU_URL


const Setting = {
    // 接口请求地址
    apiBaseURL: VUE_APP_API_URL,
    // adminsocket连接
    wsAdminSocketUrl: VUE_APP_WS_ADMIN_URL,
    // kefusocket连接
    wsKefuSocketUrl: VUE_APP_WS_KEFU_URL,
    // 路由模式，可选值为 history 或 hash
    routerMode: 'history',
    // 页面切换时，是否显示模拟的进度条
    showProgressBar: true
}

export default Setting
